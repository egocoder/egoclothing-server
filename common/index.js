const listEndpoints = require('express-list-endpoints');

showListRoutesOnConsole = (app, port) => {
  const listRoues = listEndpoints(app);
  listRoues.forEach(route => {
    let routeString = `${route.methods} - http://localhost:${port}${route.path}\n`;
    var color = '';
    switch(route.methods[0]) {
      
      case 'GET':
        color = '\x1b[31m'
        break;
      case 'POST':
        color = '\x1b[32m'
        break;
      case 'PUT':
        color = '\x1b[33m'
        break;
      case 'DELETE':
        color = '\x1b[34m'
        break;
    } 
    console.log(color, routeString);
  });
}

module.exports = {
  showListRoutesOnConsole
}