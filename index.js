const express = require("express");
const bodyParser = require("body-parser");
const fileUpload = require('express-fileupload');
const cors = require('cors');
const app = express();

const { showListRoutesOnConsole } = require('./common');

const port = process.env.port || 3333;

// SUPPORT BODY READER
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());
app.use(fileUpload());

// SUPPORT SERVER STATIC VIEW
app.set("view engine", "ejs");
app.use('/public', express.static(__dirname + '/public'));

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
  res.header('Access-Control-Allow-Headers: X-Requested-With');
  res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE");
  next();
});

// ROUTES
const index = require('./routes/index');
const account = require('./routes/account');
const product = require('./routes/product');
const category = require('./routes/category');
const brand = require('./routes/brand');
const business = require('./routes/business');
const order = require('./routes/order');
const comment = require('./routes/comment');
const status = require('./routes/status');

// MAP ROUTES
app.use('/', index);
app.use('/api/v1/account', account);
app.use('/api/v1/category', category);
app.use('/api/v1/brand', brand);
app.use('/api/v1/product', product);
app.use('/api/v1/business', business);
app.use('/api/v1/order', order);
app.use('/api/v1/comment', comment);
app.use('/api/v1/status', status);

// SHOW ROUTES REGISTERED
showListRoutesOnConsole(app, port);

app.listen(port, () => {
  console.log(`EGoClothing API is running on port ${port}`);
})