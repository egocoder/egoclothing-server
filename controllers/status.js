var statusModel = require('../models/status');
let { RES_DATA_SUCCESS, RES_DATA_FAIL, INVALID, EXIST, NOT_EXIST, NOT_AVAILABLE } = require('./response');

// find all
findStatusesController = (req, res) => {
    statusModel.findStatuses()
        .then(data => res.json(RES_DATA_SUCCESS('find categories success', 200, data)))
        .catch(err => res.json(RES_DATA_FAIL('find categories fail', 400, err)))
};

module.exports = {
    findStatusesController,
}