var commentModel = require('../models/comment');

// create comment
createCommentController = (req, res) => {
    let comment = req.body;
    commentModel.createComment(comment)
        .then(data => res.json(RES_DATA_SUCCESS('create comment success', 201, data)))
        .catch(err => res.json(RES_DATA_FAIL('create comment fail', 400, err)))
};

// find all
findCommentsController = (req, res) => {
    commentModel.findComments()
        .then(data => res.json(RES_DATA_SUCCESS('find comments success', 200, data)))
        .catch(err => res.json(RES_DATA_FAIL('find comments fail', 400, err)))
};
// find by id
findCommentByIdController = (req, res) => {
    commentModel.findCommentById(req.params.comment_id)
        .then(data => res.json(RES_DATA_SUCCESS('find comments by id success', 200, data)))
        .catch(err => res.json(RES_DATA_FAIL('find comments by id fail', 400, err)))
};

// find by product_id
findCommentByProductIdController = (req, res) => {
    commentModel.findCommentByProductId(req.params.Product_Id)
        .then(data => res.json(RES_DATA_SUCCESS('find comments by id success', 200, data)))
        .catch(err => res.json(RES_DATA_FAIL('find comments by id fail', 400, err)))
};
module.exports = {
    createCommentController,
    findCommentsController,
    findCommentByIdController,
    findCommentByProductIdController,
}