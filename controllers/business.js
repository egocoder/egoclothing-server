let { RES_DATA_FAIL } = require('./response');
const nodemailer = require('nodemailer');

let path = require('path');
let appDir = path.dirname(require.main.filename);

// upload image
uploadImageController = (req, res) => {
  let imageFile = req.files.file;

  if (!imageFile) {
    return res.status(200).send({
      message: "upload image success",
      description: "not include image"
    })
  }
  
  imageFile.mv(`${appDir}/public/imgs/${req.body.filename}.jpg`, (err) => {
    if (err) {
      return res.json(RES_DATA_FAIL(500, 'upload image fail', 'upload image fail'))
    }
    res.json({
      file: `public/imgs/${req.body.filename}.jpg`
    });
  });
};

sendEmailController = (req, res) => {
  
  let { receiveMailers, content, subject } = req.body;

  var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: 'kietphototailieu@gmail.com',
      pass: 'concaccon'
    }
  });
  
  var mailOptions = {
    from: '"EGOCLOTHING - SUPPORT" <support@egoclothing.com>',
    to: receiveMailers,
    subject,
    html: content
  };
  transporter.sendMail(mailOptions, function(error, info){
    if (error) {
      console.log(error);
    } else {
      console.log('Email sent: ' + info.response);
    }
  });
  res.send({
    "API": "NODEMAILER",
    "STATUS": true,
    "MESSAGE": "SEND MAIL SUCCESSED"
  })
}

module.exports = {
  uploadImageController,
  sendEmailController
}