let express = require('express');
let router = express.Router();
let status = require('../controllers/status');

// GET ALL CATEGORY
router.get('/',
  status.findStatusesController);

module.exports = router;