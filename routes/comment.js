let express = require('express');
let router = express.Router();
let comment = require('../controllers/comment');

// GET ALL CATEGORY
router.get('/',
  comment.findCommentsController);

// GET CATEGORY BY ID
router.get('/:comment_id',
  comment.findCommentByIdController);

// GET CATEGORY BY Product ID
router.get('/ByProduct/:Product_Id',
  comment.findCommentByProductIdController);

// CREATE CATEGORY
router.post('/',
  comment.createCommentController);
  
module.exports = router;