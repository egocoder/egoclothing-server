let express = require('express');
let router = express.Router();
let brand = require('../controllers/brand');
let mw = require('../controllers/middle_ware');

// GET ALL brand
router.get('/',
  brand.findBrandsController);

// GET brand BY ID
router.get('/:brand_id',
  brand.findBrandByIdController);

// CREATE brand
router.post('/',
  mw.verifyTokenController,
  mw.verifyAdminController,
  brand.createBrandController);

// UPDATE brand
router.put('/:brand_id',
  mw.verifyTokenController,
  mw.verifyAdminController,
  brand.updateBrandController);

// DELETE brand
router.delete('/:brand_id',
  mw.verifyTokenController,
  mw.verifyAdminController,
  brand.deleteBrandController);
module.exports = router;