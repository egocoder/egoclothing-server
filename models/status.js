var db = require('./DB_Manager');

// find all
findStatuses = () => {
    return new Promise((resolve, reject) => {
      db.executeQuery("select * from status",
        (err, data) => {
          if (err) reject(err)
          resolve(data)
        }
      );
    })
  }

module.exports = {
    findStatuses,
  }