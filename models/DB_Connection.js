const mysql = require('mysql');
const config = require('config');

const DB_CONFIG_LOCAL = config.get('Local.dbConfig');
// let DB_CONFIG_PRODUCTION = config.get('Production.dbConfig');


const pool      =    mysql.createPool(DB_CONFIG_LOCAL);
module.exports = pool;