var db = require('./DB_Manager');

// create category
createComment = (comment) => {
  return new Promise((resolve, reject) => {
    db.executeQuery("insert into comment (Content, Comment_IsRemoved, Account_Id, Product_Id) values (?, ?, ?, ?)", 
    [comment.Content, 0, comment.Account_Id, comment.Product_Id],
      (err, data) => {
        if (err) reject(err)
        resolve(data)
      }
    );
  })
}
// read category
// find all
findComments = () => {
    return new Promise((resolve, reject) => {
      db.executeQuery("select * from comment where Comment_IsRemoved = 0",
        (err, data) => {
          if (err) reject(err)
          resolve(data)
        }
      );
    })
  }
  // find by id
findCommentById = (id) => {
    return new Promise((resolve, reject) => {
      db.executeQuery("select * from comment where Comment_Id = ?", id,
        (err, data) => {
          if (err) reject(err)
          resolve(data)
        }
      );
    })
  }

  findCommentByProductId = (product_id) => {
    return new Promise((resolve, reject) => {
      db.executeQuery("select * from comment where Product_Id = ?", product_id,
        (err, data) => {
          if (err) reject(err)
          resolve(data)
        }
      );
    })
  }

module.exports = {
    createComment,
    findComments,
    findCommentById,
    findCommentByProductId,
  }